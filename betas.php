<?php
/**
 * Template Name: Betas
 */

 //* Force Full Width Layout
 add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

 //* Remove default loop and replace with custom loop
 remove_action('genesis_loop', 'genesis_do_loop');
 add_action('genesis_loop', 'upl_custom_loop');

 function upl_custom_loop() {
 ?>
     <section class="wrap intro">
         <div class="intro-left">kkkkk
             <h1><?php the_field('hero_title'); ?></h1>
             <?php if( get_field('hero_subtitle') ): ?>
               <p><?php the_field('hero_subtitle'); ?></p>
             <?php endif; ?>
               <?php the_field('hero_content'); ?>
             <a href="<?php the_field ('hero_button_link'); ?>" class="button"><?php the_field ('hero_button'); ?></a>

         </div>
         <div class="intro-right">
             <div class="checklist">
               <div class="checklist-header text-center">
                 <img src="<?php the_field('checklist_image')['url']; ?>" alt="<?php the_field('checklist_image')['alt']; ?>" />
                 <ul class="checklist-heading">
                   <?php if( have_rows('checklist_heading') ): ?>

                  <?php while( have_rows('checklist_heading') ): the_row();
                    ?>
                      <li>
                        <?php the_sub_field('heading_word'); ?>
                      </li>
                  <?php endwhile; ?>

                  <?php endif; ?>
                 </ul>
               </div>
               <?php if( have_rows('hero_checkpoints') ): ?>

              <ul class="check-list">
                <?php while( have_rows('hero_checkpoints') ): the_row();
                  // vars
                  $listItem = get_sub_field('list_item');
                  ?>
                  <li>
                      <?php echo $listItem; ?>
                  </li>
                <?php endwhile; ?>
              </ul>

              <?php endif; ?>
             </div>
         </div>
     </section>
     <section class="wrap">
       <h2 class="text-center"><?php the_field('who_uses_heading'); ?></h2>
       <div class="flex-groups">
         <?php if( have_rows('client_groups') ): ?>

        <?php while( have_rows('client_groups') ): the_row();
          ?>
            <div class="flex-item text-center">
              <img src="<?php the_sub_field('group_icon')['url']; ?>" alt="<?php echo the_sub_field('group_icon')['alt']; ?>" />
              <h2><?php the_sub_field('group_title') ?></h2>
              <?php the_sub_field('group_description');?>
            </div>
        <?php
        endwhile; 
        ?>
      <?php
         endif; 
      ?>
       </div>
     </section>
     <section class="wrap">
       <div class="text-center">
         <h2><?php the_field('featured_heading');?></h2>
         <?php the_field('featured_text');?>
       </div>
       <?php
         $images = get_field('featured_gallery');

         if( $images ): ?>
             <ul class="gallery text-center">
                 <?php foreach( $images as $image ): ?>
                     <li>
                         <a href="<?php echo $image['url']; ?>">
                              <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                         </a>
                     </li>
                 <?php endforeach; ?>
             </ul>
         <?php endif; ?>
     </section>
 <?php }

 genesis();
