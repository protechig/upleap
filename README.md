# Genesis Utility Theme

GitLab project link: https://gitlab.com/protechig/genesis-utility

## Project Overview

The Genesis Utility theme is a project for theme developers to speed up development time using the latest and greatest web technologies. Unlike the theme that this is inspired by, Genesis Sample, Genesis utility uses:

- Gulp
- Sass
- Bourbon
- Neat
- Bitters

And other tools to speed things up. Genesis Utility also uses Flexbox for layouts. While Flexbox is only supported by modern browsers, using tools like 10up's [Flexibility](https://github.com/10up/flexibility), we can backport support all the way down to IE 8.

### Getting Your Development Environment Set Up

You should be running WordPress using whatever tool you prefer. You then need to install Node JS and Ruby.

Within the theme's directory type `npm install` to install to intall required node dependencies for Gulp.

To install Bourbon & Neat type `gem install bourbon neat`.

Then navigate to the `sass/` directory and type in `bourbon install` and `neat install` to install the latest version of Bourbon and Neat to the theme.

In the theme's root directory type `gulp watch` to run the gulp task that looks for changes in Sass and compiles when recognized.

You should then be good to go!


## Installation Instructions

1. Upload the Genesis Sample theme folder via FTP to your wp-content/themes/ directory. (The Genesis parent theme needs to be in the wp-content/themes/ directory as well.)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Genesis Utility Theme theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.


## Theme Support

Please visit http://zachrussell.net for theme support.
