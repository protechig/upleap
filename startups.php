<?php
/**
 * Template Name: Startups
 */

 //* Force Full Width Layout
 add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

 //* Remove default loop and replace with custom loop
 remove_action('genesis_loop', 'genesis_do_loop');
 add_action('genesis_loop', 'upl_custom_loop');

 function upl_custom_loop() {
 ?>
     <section class="intro">
        <div class="wrap">
             <h1><?php the_field('hero_title'); ?></h1>
             <?php if( get_field('hero_subtitle') ): ?>
               <p><?php the_field('hero_subtitle'); ?></p>
             <?php endif; ?>
               <?php the_field('hero_content'); ?>
             <a href="<?php the_field ('hero_button_link'); ?>" class="button"><?php the_field ('hero_button'); ?></a>
        </div>
     </section>
        <section class="how-it-works">
            <div class="wrap">
             <h2><?php the_field('how_heading');?></h2>
             <div class="how-text"><?php the_field('how_text');?></div>
             <?php if(have_rows('how_it_works')): ?>
                <?php $count = 1; ?>
                <?php while(have_rows('how_it_works')) : the_row(); ?>

                    <div class="step">
                        <span class="number"><?php echo $count; ?></span>
                        <?php the_sub_field('step_description'); ?>
                    </div>
                    <?php $count++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
        </section>


     <section class="how-works">
        <div class="wrap">
               <div class="flex-groups">
         <?php if( have_rows('how_it_works_content') ): ?>

         	<?php while( have_rows('how_it_works_content') ): the_row();
         		?>
             <div class="flex-item category">
                 <img src="<?php the_sub_field('how_icon')['url']; ?>" alt="<?php the_sub_field('how_icon')['alt']; ?>" />
                 <h3 class="text-center"><?php the_sub_field('how_heading') ?></h3>
                 <?php the_sub_field('how_content');?>
             </div>
            <?php endwhile; ?>
          <?php endif; ?>
       </div>
        </div>
     </section>
     <section class="partners">
        <div class="wrap">
            <h2><?php the_field('our_partners_heading');?></h2>
            <!--Partner Badge Slider Here-->
    <div class="partners-list">
     <?php

        // check if the repeater field has rows of data
        if( have_rows('partner_logo') ):

            // loop through the rows of data
            while ( have_rows('partner_logo') ) : the_row();
            ?>     
            <div class="logo">
             <!--display a sub field value-->
             <img src="<?php the_sub_field('logo')['url']; ?>" alt="<?php echo the_sub_field('logo')['alt']; ?>" /> 
             <p><?php the_sub_field('partner_name'); 
              ?> </p>
            </div>  
            <?php
            endwhile;

        else :

            // no rows found

        endif;

        ?>
</div>
        </div>
    </section>
      <!-- <section class="wrap text-center">
         <h2><?php the_field('logo_header');?></h2>

           	<img src="<?php the_field('startup_image')['url']; ?>" alt="<?php the_field('startup_image')['alt']; ?>" />

     </section> -->
 <?php }

 genesis();
