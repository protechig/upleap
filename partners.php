<?php
/**
 * Template Name: Partners
 */

 //* Force Full Width Layout
 add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

 //* Remove default loop and replace with custom loop
 remove_action('genesis_loop', 'genesis_do_loop');
 add_action('genesis_loop', 'upl_custom_loop');

 function upl_custom_loop() {
 ?>
     <section class="intro">
        <div class="wrap">
             <h1><?php the_field('hero_title'); ?></h1>
             <?php if( get_field('hero_subtitle') ): ?>
               <p><?php the_field('hero_subtitle'); ?></p>
             <?php endif; ?>
               <?php the_field('hero_content'); ?>
             <a href="<?php the_field ('hero_button_link'); ?>" class="button first"><?php the_field ('hero_button'); ?></a>
             <a href="<?php the_field ('challenge_button_link'); ?>" class="button magenta" ><?php the_field ('challenge_button'); ?></a>


        </div>
     </section>
        <section class="how-it-works">
            <div class="wrap">
             <h2><?php the_field('how_it_works_heading');?></h2>
             <div class="intro"><?php the_field('how_it_works_description');?></div>
            <?php if(have_rows('how_it_works')): ?>
                <?php $count = 1; ?>
                <?php while(have_rows('how_it_works')) : the_row(); ?>

                    <div class="step">
                        <span class="number"><?php echo $count; ?></span>
                        <?php the_sub_field('step_description'); ?>
                    </div>
                    <?php $count++; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
        </section>

        <section class="challenges">
            <div class="wrap">
             <h2>OR</h2>
            <?php if(have_rows('challenges')): ?>
                <?php while(have_rows('challenges')) : the_row(); ?>

                    <div class="step">
                        <span class="number" style="color: #d62282; border: 3px solid #d62282" ><?php echo $count; ?></span>
                        <div class="step-description">
                          <?php the_sub_field('step_description'); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
        </section>


     <section class="how-works">
        <div class="wrap">
            <h2><?php the_field('services_heading'); ?></h2>
            <div class="intro"><?php the_field('services_description'); ?></div>
               <div class="flex-groups">
         <?php if( have_rows('how_it_works_content') ): ?>

         	<?php while( have_rows('how_it_works_content') ): the_row();
         		?>
             <div class="flex-item category">
                 <img src="<?php the_sub_field('how_icon')['url']; ?>" alt="<?php the_sub_field('how_icon')['alt']; ?>" />
                 <h3 class="text-center"><?php the_sub_field('how_heading') ?></h3>
                 <?php the_sub_field('how_content');?>
             </div>
            <?php endwhile; ?>
          <?php endif; ?>
       </div>
        </div>
     </section>
     <section class="text-center">
        <div class="wrap">
       <h2>Our Partners</h2>
        // Partner Badge(s) Here
        <?php //upl_agency_card(142); ?>
        </div>
     </section>
      <!-- <section class="wrap text-center">
         <h2><?php the_field('logo_header');?></h2>

           	<img src="<?php the_field('startup_image')['url']; ?>" alt="<?php the_field('startup_image')['alt']; ?>" />

     </section> -->
 <?php }

 genesis();
