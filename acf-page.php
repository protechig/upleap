<?php
/**
 * Template name: Global ACF Page Template
 */

//* Force Full Width Layout
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

//* Remove default loop and replace with custom loop
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'upl_custom_loop');

function upl_custom_loop() {
?>


<div class="container">
<?php
	if( have_rows('upleap_fields')):
		while(have_rows('upleap_fields')): the_row();
			get_template_part('content-blocks/block-' . get_row_layout() );
		endwhile;
	endif;
?>
</div>

        <?php }

genesis();