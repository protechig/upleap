<?php
/**
 * Template Name: Startups
 */

//* Force Full Width Layout
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

//* Remove default loop and replace with custom loop
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'upl_custom_loop');

function upl_custom_loop() {
?>
    <section class="wrap intro">
        <div class="intro-left">
            <h1><?php the_field('hero_title'); ?></h1>
            <?php if( get_field('hero_subtitle') ): ?>
              <h2><?php the_field('hero_subtitle'); ?></h2>
            <?php endif; ?>
              <p><?php the_field('hero_content'); ?></p>

        </div>
        <div class="intro-right">
            <div class="checklist">
              <div class="checklist-header text-center">
                <img src="<?php the_field('checklist_image')['url']; ?>" alt="<?php the_field('checklist_image')['alt']; ?>" />
                <ul class="checklist-heading">
                  <?php if( have_rows('checklist_heading') ): ?>

                   <?php while( have_rows('checklist_heading') ): the_row();
                     ?>
                      <li>
                        <?php the_sub_field('heading_word'); ?>
                      </li>
                   <?php endwhile; ?>

                  <?php endif; ?>
                </ul>
              </div>
              <?php if( have_rows('hero_checkpoints') ): ?>

               <ul class="check-list">
                 <?php while( have_rows('hero_checkpoints') ): the_row();
                   // vars
                   $listItem = get_sub_field('list_item');
                   ?>
                   <li>
                       <?php echo $listItem; ?>
                   </li>
                 <?php endwhile; ?>
               </ul>

              <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="wrap">
      <div class="flex-groups">
        <?php if( have_rows('services') ): ?>

         <?php while( have_rows('services') ): the_row();
           ?>
            <div class="flex-item text-center">
              <img src="<?php the_sub_field('service_icon')['url']; ?>" alt="<?php echo the_sub_field('service_icon')['alt']; ?>" />
              <h2><?php the_sub_field('service_title') ?></h2>
              <?php the_sub_field('service_description');?>
              <a class="button" href="<?php the_sub_field('service_link'); ?>"><?php the_sub_field('button_text'); ?></a>
            </div>
           <?php endwhile; ?>
         <?php endif; ?>
      </div>
    </section>
<?php }

genesis();
