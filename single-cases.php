<?php
/**
 * Case Study Template
 */

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'upl_custom_loop');
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

function upl_custom_loop() {
    // Add your code here
    ?>
    <section class="special">
        <div class="wrap">
        <div class="heading">
        <h1 class="title">
        <?php the_field('heading'); ?>
        </h1>
        <p class="sub-title"><?php the_field('sub_title'); ?></p>
        </div>
            <div class="case">

                <?php 
            $services = get_field('services_offered');
            foreach ($services as $service) {
                if ($service['value'] == 'prototyping') {
                ?>
                <div class="case-study">
                    <h2 class="service-label">
                        <?php echo $service['label']; ?>
                    </h2>
                    <img src="<?php echo get_stylesheet_directory_uri() . '/images/service-icon2.png'; ?>">
                </div>
                <?php

                } elseif ($service['value'] == 'web_development') {
                    ?>
                    <div class="case-study">
                       <h2 class="service-label">
                            <?php echo $service['label']; ?>
                        </h2>
                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/service-icon.png'; ?>">
                    </div>
                    <?php
                } elseif ($service['value'] == 'emerging_tech') {
                    ?>
                        <div class="case-study">
                          <h2 class="service-label">
                                <?php echo $service['label']; ?>
                            </h2>
                            <img src="<?php echo get_stylesheet_directory_uri() . '/images/Icon.png'; ?>">
                        </div>
                        <?php
                }elseif ($service['value']  == 'design'){
                    ?>
                            <div class="case-study">
                               <h2 class="service-label">
                                    <?php echo $service['label']; ?>
                                </h2>
                                <img src="<?php echo get_stylesheet_directory_uri() . '/images/Group-17.png'; ?>">
                            </div>
                            <?php
                }elseif ($service['value']  == 'analytics'){
                    ?>
                                <div class="case-study">
                                   <h2 class="service-label">
                                        <?php echo $service['label']; ?>
                                    </h2>
                                    <img src="<?php echo get_stylesheet_directory_uri() . '/images/Group-20.png'; ?>">
                                </div>
                                <?php
                }elseif ($service['value']  == 'marketing'){
                    ?>
                                    <div class="case-study">
                                       <h2 class="service-label">
                                            <?php echo $service['label']; ?>
                                        </h2>
                                        <img src="<?php echo get_stylesheet_directory_uri() . '/images/Group-19-1.png'; ?>">
                                    </div>
                                    <?php
                }
            }

            ?>

            </div>
        </div>
    </section>

    <div class="wrap">
        <div class="case-studies">
            <?php if( have_rows('case_study_content') ): ?>

            <?php while( have_rows('case_study_content') ): the_row();
                echo '<div class="case-study">';
                 ?>
            <div class="case-content">
                <h3>
                    <?php the_sub_field('section_title'); ?>
                </h3>
                <p>
                    <?php the_sub_field('section_content'); ?>
                </p>
            </div>
            <div class="case-image">
                <img src="<?php echo the_sub_field('right_image'); ?>">
            </div>
        </div>
        <!-- .case-study -->
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
    </div>

    <section class="partners">
        <div class="wrap">
            <h2>
                <?php the_field('our_partners');?>
            </h2>
            <!--Partner Badge Slider Here-->
            <div class="partners-list">
                <?php
        // check if the repeater field has rows of data
        if( have_rows('partners_logo') ):
            // loop through the rows of data
            while ( have_rows('partners_logo') ) : the_row();
            ?>
                    <div class="logo">
                        <!--display a sub field value-->
                        <img src="<?php the_sub_field('logo')['url']; ?>" alt="<?php echo the_sub_field('logo')['alt']; ?>" />
                        <p>
                            <?php the_sub_field('partner_name'); 
              ?> </p>
                    </div>
                    <?php
            endwhile;

        else :

            // no rows found

        endif;

        ?>
            </div>
        </div>
    </section>
    <?php
}

 genesis();

?>
