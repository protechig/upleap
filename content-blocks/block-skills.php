<section class="skills">
        <div class="wrap">
            <h2 class="text-center"><?php the_sub_field('skills_title'); ?></h2>
            <p class="text-center"><?php the_sub_field('tittle_description'); ?></p>
            <div class="flex-groups">
                <?php if( have_rows('skills') ): ?>

                <?php while( have_rows('skills') ): the_row();
         		?>
                <div class="flex-item category">
                    <img src="<?php the_sub_field('image')['url']; ?>" alt="<?php the_sub_field('image')['alt']; ?>" />
                    <h2 class="text-center">
                        <?php the_sub_field('title') ?>
                    </h2>
                        <?php the_sub_field('list');?>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>