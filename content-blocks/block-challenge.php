<div class="challenges">
		<div class="wrap">
			<h2>
			<img src="<?php the_sub_field( 'challenge_icon' ); ?>" />
				<?php the_sub_field( 'challenges_heading' ); ?>
			</h2>
			<div class="challenges-info">
			   <h3><?php the_sub_field( 'challenges_text' ); ?></h3>
			   <?php if ( get_sub_field( 'button_link' ) ) : ?>
					<a href="<?php the_sub_field( 'button_link' ); ?>" class="button magenta">
						<?php if ( get_sub_field( 'button_text' ) ) : ?>
							<?php the_sub_field( 'button_text' ); ?>
						<?php endif; ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
</div>
