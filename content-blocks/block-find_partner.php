<section class="find-partner"><!--find a partner  section-->
<div class="wrap find-partner-container">

          <div class="left-content">
          <div>
           <h1><?php the_sub_field('header'); ?></h1>
           <h3 class="text-tittle"><?php the_sub_field('content_tittle'); ?></h3>
            <?php
               the_sub_field('description_content'); 
            ?>
            </div>
            <button class="cta-partner"><a href="<?php the_sub_field('button_url'); ?>"><?php the_sub_field('button_text'); ?></a></button>
        </div>
        <div class="right-image">
            <img class="fifty-image" src="<?php the_sub_field('featured_image'); ?>" alt="<?php the_sub_field('featured_image'); ?>">
        </div>

    </div><!-- .grid-x -->
</section>