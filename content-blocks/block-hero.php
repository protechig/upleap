<section class="intro">
        <div class="wrap">
            <div class="intro-content">
                <?php the_sub_field('hero_title'); ?>
                <?php if(get_sub_field('hero_subtitle')) : ?>
                    <?php the_sub_field('hero_subtitle'); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>