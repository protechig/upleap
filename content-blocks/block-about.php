<section class="about">
		<div class="wrap">
			<h2><span class="blue-underline"><?php the_sub_field( 'about_title' ); ?></span><?php the_sub_field( 'about_title2' ); ?></h2>
				<?php the_sub_field( 'about' ); ?>
		 <?php if ( get_field( 'button' ) ) : ?>
			<a class="button secondary" href="<?php the_sub_field( 'button_link' ); ?>">
				<?php the_sub_field( 'button' ); ?>
			</a>
			<?php endif; ?>
	</section>
