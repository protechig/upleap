<div class="newsletter">
	<div class="wrap">
        <div class="newsletter-form">
            <div class="header">
                <h4><?php the_sub_field('form_header'); ?></h4>
            </div>
            <div class="form">
                <?php
                    $form = get_sub_field( 'signup_form' );
                    gravity_form( $form, false, true, false, '', true, 1 );
                ?>
            </div>
        </div>
    </div>
</div>
