<section class="services">
        <div class="wrap">
            <h1 style="text-align: center;"><?php the_sub_field('service_header') ?></h1>
            <div class="flex-groups">
                <?php if( have_rows('services') ): ?>

                <?php while( have_rows('services') ): the_row();
                    ?>
                <div class="flex-item service text-center">
                    <img src="<?php the_sub_field('service_icon')['url']; ?>" alt="<?php echo the_sub_field('service_icon')['alt']; ?>" />
                    <h3>
                        <?php the_sub_field('service_title') ?>
                    </h3>
                        <?php the_sub_field('service_description'); ?>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>