<section class="how-works"><!--how it works section-->
        <div class="wrap">
            <h2><?php the_sub_field('how_it_works_title'); ?></h2>
            <?php if(have_rows('how_it_works')): ?>
            <?php $count = 1; ?>
            <?php while(have_rows('how_it_works')) : the_row(); ?>

            <div class="step">
                <span class="number" style="color: <?php the_sub_field('challenge_color'); ?>; border: 3px solid <?php the_sub_field('challenges_color'); ?>"><?php echo $count; ?></span>
                <div class="procedures">
                <h3 style="color: <?php the_sub_field('challenge_color');?>"><?php the_sub_field('sub_heading');?></h3>
                <?php the_sub_field('step_description'); ?>
                </div>
            </div>
            <?php $count++; ?>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </section>