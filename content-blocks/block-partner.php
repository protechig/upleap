<section class="partners">
        <div class="wrap">
            <h2>
             <?php the_field('our_partners_heading');?>
            </h2>
            <!--Partner Badge Slider Here-->
            <div class="partners-list">
                <?php
        // check if the repeater field has rows of data
        if( have_rows('partner_logo') ):
            // loop through the rows of data
            while ( have_rows('partner_logo') ) : the_row();
            ?>
                    <div class="logo">
                    <!--display a sub field value-->
                    <img src="<?php the_sub_field('logo')['url'];?>" alt="<?php echo the_sub_field('logo')['alt']; ?>" />
                    <p><?php the_sub_field('company_name'); ?> </p>
                </div>
                    <?php
            endwhile;
        else :

            // no rows found

        endif;

        ?>
            </div>
        </div>
    </section>