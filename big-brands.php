<?php
/**
 * Template Name: Big Brands
 */

//* Force Full Width Layout
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

//* Remove default loop and replace with custom loop
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'upl_custom_loop');

function upl_custom_loop()
{
    ?>
    <section class="intro">
        <div class="wrap">
            <h1>
                <?php the_field('hero_title');?>
            </h1>
            <?php if (get_field('hero_subtitle')): ?>
            <p>
                <?php the_field('hero_subtitle');?>
            </p>
            <?php endif;?>
            <?php the_field('hero_content');?>
            <a href="<?php the_field('hero_button_link');?>" class="button">
                <?php the_field('hero_button');?>
            </a>
            <a href="<?php the_field('challenge_button_link');?>" class="button" style="background: <?php the_field('challenges_color');?>">
                <?php the_field('challenge_button');?>
            </a>

        </div>
    </section>
    <section class="how-it-works">
        <div class="wrap">
            <h2>
                <?php the_field('how_heading');?>
            </h2>
            <?php the_field('how_text');?>
            <?php if (have_rows('how_it_works')): ?>
            <?php $count = 1;?>
            <?php while (have_rows('how_it_works')): the_row();?>

            <div class="step">
                <span class="number"><?php echo $count; ?></span>
                <?php the_sub_field('step_description');?>
            </div>
            <?php $count++;?>
            <?php endwhile;?>
        <?php endif;?>
        </div>
    </section>

    <section class="challenges">
        <div class="wrap">
            <h2>OR</h2>
            <?php if (have_rows('challenges')): ?>
            <?php $count = 1;?>
            <?php while (have_rows('challenges')): the_row();?>

            <div class="step">
                <span class="number" style="color: <?php the_field('challenges_color');?>; border: 3px solid <?php the_field('challenges_color');?>"><?php echo $count; ?></span>
                <?php the_sub_field('step_description');?>
            </div>
            <?php $count++;?>
            <?php endwhile;?>
            <?php endif;?>
        </div>
    </section>


    <section class="skilled">
        <div class="wrap">
            <div class="flex-groups">
                <?php if (have_rows('skills')): ?>

                <?php while (have_rows('skills')): the_row();
        ?>
                <div class="flex-item category">
                    <img src="<?php the_sub_field('image')['url'];?>" alt="<?php the_sub_field('image')['alt'];?>" />
                    <h2 class="text-center">
                        <?php the_sub_field('title')?>
                    </h2>
                    <p>
                        <?php the_sub_field('list');?>
                    </p>
                </div>
                <?php endwhile;?>
            <?php endif;?>
            </div>
        </div>
    </section>



    <section class="text-center">
        <div class="wrap">
            <h2><?php the_field('partner_heading');?></h2>
            <!-- Partner Badge(s) Here-->
            <div class="partners-list">
                <?php

    // check if the repeater field has rows of data
    if (have_rows('partner_logo')):

        // loop through the rows of data
        while (have_rows('partner_logo')): the_row();
            ?>
                        <div class="logo">
                            <!--display a sub field value-->
                            <img src="<?php the_sub_field('logo')['url'];?>" alt="<?php echo the_sub_field('logo')['alt']; ?>" />
                            <p><?php the_sub_field('company_name'); ?> </p>
                        </div>
                        <?php
endwhile;

    else:

        // no rows found

    endif;

    ?>
            </div>
            <?php //upl_agency_card(142); ?>
        </div>
    </section>
    <!-- <section class="wrap text-center">
         <h2><?php //the_field('logo_header');?></h2>

	<img src="<?php //the_field('startup_image')['url'];?>" alt="<?php //the_field('startup_image')['alt'];?>" />

     </section> -->
    <?php }

genesis();
