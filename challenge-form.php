<?php
/**
 * Template Name: Challenge Form
 */

 // * Force Full Width Layout
 add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

 // * Remove default loop and replace with custom loop
 add_action( 'genesis_loop', 'upl_custom_loop' );

 function upl_custom_loop() {
	?>
	 <div class="container">
   <?php
	if ( have_rows( 'upleap_fields' ) ) :
		while ( have_rows( 'upleap_fields' ) ) :
			the_row();
			get_template_part( 'content-blocks/block-' . get_row_layout() );
		endwhile;
	endif;
	?>
  </div>
		<div class="challenge-form">
	<?php
	$form = get_field( 'challenge_form' );
	gravity_form( $form, false, true, false, '', true, 1 );
	?>
		</div>


	<div class="disclaimer">
		<p>
	<?php the_field( 'disclaimer' ); ?> </p>
	</div>


	<?php
	}

 genesis();
