var gulp = require('gulp'),
        sass = require('gulp-sass')
    autoprefixer = require('gulp-autoprefixer')
    input = './sass/**/*.scss'
    output = '.'


var sassOptions = {
        errLogToConsole: true,
        outputStyle: 'expanded'

}
var autoprefixerOptions = {
        browsers: ['last 2 versions', '> 5%', 'Firefox ESR']

}


gulp.task('sass', function() {
        return gulp
            .src(input)
            .pipe(sass(sassOptions).on('error', sass.logError)) //using gulp-sass
            .pipe(autoprefixer(autoprefixerOptions))
            .pipe(gulp.dest(output))

});

gulp.task('watch', function() {
        return gulp
        // Watch input folder for change and run sass if something happens
            .watch(input, ['sass'])

        // When there is a change
         .on('change', function(event) {
             console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    })
});

gulp.task('default', ['sass', 'watch']);
