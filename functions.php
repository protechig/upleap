<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

// * Start the engine
require_once get_template_directory() . '/lib/init.php';

// * Setup Theme
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

// * Set Localization (do not remove)
load_child_theme_textdomain( 'genesis-sample', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'genesis-sample' ) );

// * Add Image upload and Color select to WordPress Theme Customizer
require_once get_stylesheet_directory() . '/lib/customize.php';

// * Include Customizer CSS
require_once get_stylesheet_directory() . '/lib/output.php';

define( 'CHILD_THEME_NAME', 'UpLeap' );
define( 'CHILD_THEME_URL', 'https://protechig.com/' );
define( 'CHILD_THEME_VERSION', '2.2.3' );

// * Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style( 'google-font', '//fonts.googleapis.com/css?family=Roboto:400,400i,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );
	$output = array(
		'mainMenu' => __( 'Menu', 'genesis-sample' ),
		'subMenu'  => __( 'Menu', 'genesis-sample' ),
	);
	wp_localize_script( 'genesis-sample-responsive-menu', 'genesisSampleL10n', $output );

}

// * Add HTML5 markup structure
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// * Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// * Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

// * Add support for custom header
add_theme_support(
	 'custom-header', array(
		 'width'           => 600,
		 'height'          => 160,
		 'header-selector' => '.site-title a',
		 'header-text'     => false,
		 'flex-height'     => true,
	 )
	);

// * Add support for custom background
add_theme_support( 'custom-background' );

// * Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

// * Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );

// * Add Image Sizes
add_image_size( 'featured-image', 720, 400, true );

// * Rename primary and secondary navigation menus
add_theme_support(
	 'genesis-menus', array(
		 'primary'   => __( 'After Header Menu', 'genesis-sample' ),
		 'secondary' => __( 'Footer Menu', 'genesis-sample' ),
	 )
	);

// * Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// * Reduce the secondary navigation menu to one level depth
add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}

// * Modify size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

// * Modify size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

// //Install ACF
// // 1. Customize ACF Path
// add_filter('acf/settings/path', 'upleap_acf_settings_path');
// function upleap_acf_settings_path( $path ) {
// update path
// $path = get_stylesheet_directory() . '/acf/';
// return $path;
// }
// // 2. Customize ACF dir
// add_filter('acf/settings/dir', 'upleap_acf_settings_dir');
// function upleap_acf_settings_dir( $dir ) {
// update path
// $dir = get_stylesheet_directory_uri() . '/acf/';
// return $dir;
// }
// // 3. Hide ACF field group menu item
// //add_filter('acf/settings/show_admin', '__return_false');
// // 4. Include ACF
// include_once( get_stylesheet_directory() . '/acf/acf.php' );
add_filter( 'acf/settings/save_json', 'upl_acf_json_save_point' );
function upl_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/acf-json';
	return $path;
}

add_filter( 'acf/settings/load_json', 'upl_acf_json_load_point' );
function upl_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/acf-json';
	return $paths;
}

// Agency Card Function
function upl_agency_card( $agency_post_id ) {
	?>
		<section class="agency-details partner-profile">
			<p>Team:</p>
			<h1><?php the_field( 'agency_name', $agency_post_id ); ?></h1>
			<?php $leader_image = get_field( 'team_leader_photo', $agency_post_id ); ?>
			<img src="<?php echo $leader_image; ?>" />
				<h3 class="leader"><?php the_field( 'agency_team_leader', $agency_post_id ); ?><a href="<?php the_field( 'agency_linkedin', $agency_post_id ); ?>" ><span class="fa fa-linkedin-square"></span></a></h3>
			<p><?php the_field( 'agency_team_leader_title', $agency_post_id ); ?> </p>
			<p class="text-left">Capabilities:</p>
		</section>

	<?php
}

// case studies
if ( ! function_exists( 'case_study' ) ) {

	// Register Custom Post Type
	function case_study() {

		$labels = array(
			'name'                  => _x( 'Case Studies', 'Post Type General Name', 'upleap' ),
			'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'upleap' ),
			'menu_name'             => __( 'Case Studies', 'upleap' ),
			'name_admin_bar'        => __( 'Case Studies', 'upleap' ),
			'archives'              => __( 'Case Archives', 'upleap' ),
			'attributes'            => __( 'Case Attributes', 'upleap' ),
			'parent_item_colon'     => __( 'Parent Item:', 'upleap' ),
			'all_items'             => __( 'All Case', 'upleap' ),
			'add_new_item'          => __( 'Add New Case', 'upleap' ),
			'add_new'               => __( 'Add Case', 'upleap' ),
			'new_item'              => __( 'New Case', 'upleap' ),
			'edit_item'             => __( 'Edit Case', 'upleap' ),
			'update_item'           => __( 'Update Case', 'upleap' ),
			'view_item'             => __( 'View Case', 'upleap' ),
			'view_items'            => __( 'View Case', 'upleap' ),
			'search_items'          => __( 'Search Case', 'upleap' ),
			'not_found'             => __( 'Not found', 'upleap' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'upleap' ),
			'featured_image'        => __( 'Case Image', 'upleap' ),
			'set_featured_image'    => __( 'Set Case image', 'upleap' ),
			'remove_featured_image' => __( 'Remove featured image', 'upleap' ),
			'use_featured_image'    => __( 'Use as Case image', 'upleap' ),
			'insert_into_item'      => __( 'Insert into item', 'upleap' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'upleap' ),
			'items_list'            => __( 'Items list', 'upleap' ),
			'items_list_navigation' => __( 'Items list navigation', 'upleap' ),
			'filter_items_list'     => __( 'Filter items list', 'upleap' ),
		);
		$args   = array(
			'label'               => __( 'Case Study', 'upleap' ),
			'description'         => __( 'add Case Study here', 'upleap' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies'          => array( 'Case study' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicon-caroot',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( 'cases', $args );

	}
	add_action( 'init', 'case_study', 0 );
}

// * Change the footer text
add_filter( 'genesis_footer_creds_text', 'sp_footer_creds_filter' );
function sp_footer_creds_filter( $creds ) {
	$creds = '';
	return $creds;
}
?>
