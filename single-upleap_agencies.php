<?php
/**

 * Template Name: Agency
 */

 //* Force Full Width Layout
 add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content');

add_action('get_header', 'acf_form_head');
 //* Remove default loop and replace with custom loop
 remove_action('genesis_loop', 'genesis_do_loop');
 add_action('genesis_loop', 'upl_custom_loop');

 function upl_custom_loop() {
 ?>
     <div class="wrap agencies">
         <div class="col-third-left">
            <?php upl_agency_card(get_the_ID()); ?>
           <section class="team-members flex-groups">
             <?php if( have_rows('team_members') ): ?>

               <?php while( have_rows('team_members') ): the_row();
                 ?>
                 <div class="partner-profile flex-item">
                   <img src="<?php the_sub_field('member_photo'); ?>" />
                   <h3><?php the_sub_field('member_name') ?></h3>
                   <p><?php the_sub_field('member_title');?></p>
                 </div>
               <?php endwhile; ?>

             <?php endif; ?>
           </section>
           <section class="agency-address">
             <h2>Address</h2>
             <?php the_field('agency_address');?>
           </section>
         </div>
         <div class="col-third-right">
           <section class="endorsements">
             <h2><span class="blue-underline">End</span>orsements</h2> <span style="vertical-align: baseline" class="fa-stack fa-sm"><i class="fa fa-comment fa-stack-2x"></i><strong class="fa-stack-1x fa-stack-text fa-inverse">+<?php echo count( get_field('endorsement_content')); ?></strong></span>
            <?php upl_agency_card(get_the_ID()); ?>
            <?php if( have_rows('endorsement_content')): ?>
                <?php while( have_rows('endorsement_content')) : the_row(); ?>
                    <div class="endorser">
                        <?php echo get_sub_field('name') . ' <a href=' . get_sub_field('linkedin_profile_link') . '><i class="fa fa-linkedin-square"></i></a> ' . get_sub_field('title') . ', ' . get_sub_field('company'); ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
           </section>
           <section class="strengths">
             <h2><span class="blue-underline">Tea</span>m Strengths/Focus</h2>
              <div class="">
               <div class="strength-category">
                 <?php if( get_field('market_strategy') ): ?>
                   <h3>Go To Market Strategy</h3>
                   <div class="strength">
                     <h4>Validation & Plan</h4>
                     <?php
                      $values = get_field('market_strategy');
                      if($values)
                      {
                      	echo '<ul>';

                      	foreach($values as $value)
                      	{
                      		echo '<li>' . $value . '</li>';
                      	}

                      	echo '</ul>';
                      }
                      ?>
                   </div>
                 <?php endif; ?>
               </div>

              <div class="strength-category">
                 <?php if( get_field('design') or get_field('development')  ): ?>
                   <h3>Design & Build</h3>
                   <?php if( get_field('design')): ?>
                     <div class="strength">
                       <h4>Design & UX</h4>
                       <?php
                        $values = get_field('design');
                        if($values)
                        {
                        	echo '<ul>';

                        	foreach($values as $value)
                        	{
                        		echo '<li>' . $value . '</li>';
                        	}

                        	echo '</ul>';
                        }
                        ?>
                     </div>
                   <?php endif; ?>
                   <?php if( get_field('development')): ?>
                     <div class="strength">
                       <h4>Web, App & Product Development</h4>
                       <?php
                        $values = get_field('development');
                        if($values)
                        {
                        	echo '<ul>';

                        	foreach($values as $value)
                        	{
                        		echo '<li>' . $value . '</li>';
                        	}

                        	echo '</ul>';
                        }
                        ?>
                     </div>
                   <?php endif; ?>
                 <?php endif; ?>
               </div>

               <div class="strength-category">
                  <?php if( get_field('brand_strategy') or get_field('marketing') or get_field('analytics')  ): ?>
                    <h3>Growth & Scale</h3>
                    <?php if( get_field('brand_strategy')): ?>
                      <div class="strength">
                        <h4>PR & Brand Strategy</h4>
                        <?php
                         $values = get_field('brand_strategy');
                         if($values)
                         {
                         	echo '<ul>';

                         	foreach($values as $value)
                         	{
                         		echo '<li>' . $value . '</li>';
                         	}

                         	echo '</ul>';
                         }
                         ?>
                      </div>
                    <?php endif; ?>
                    <?php if( get_field('marketing')): ?>
                      <div class="strength">
                        <h4>Marketing</h4>
                        <?php
                         $values = get_field('marketing');
                         if($values)
                         {
                         	echo '<ul>';

                         	foreach($values as $value)
                         	{
                         		echo '<li>' . $value . '</li>';
                         	}

                         	echo '</ul>';
                         }
                         ?>
                      </div>
                    <?php endif; ?>
                    <?php if( get_field('analytics')): ?>
                      <div class="strength">
                        <h4>Scaling up / Analytics</h4>
                        <?php
                         $values = get_field('analytics');
                         if($values)
                         {
                         	echo '<ul>';

                         	foreach($values as $value)
                         	{
                         		echo '<li>' . $value . '</li>';
                         	}

                         	echo '</ul>';
                         }
                         ?>
                      </div>
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
           </section><!--end of strengths -->

           <section class="clients">
             <h2><span class="blue-underline">Big</span> Clients</h2>
             <?php if( have_rows('clients') ): ?>
              <ul class="flex-groups">
                <?php while( have_rows('clients') ): the_row();
                  ?>
                  <li class="flex-item">
                      <?php the_sub_field('client_name'); ?>
                  </li>
                <?php endwhile; ?>
              </ul>

             <?php endif; ?>
           </section>

           <section class="case-studies">
             <h2><span class="blue-underline">Ca</span>se Studies</h2>
             <?php if( have_rows('case_study') ): ?>
              <div class="study">
                <?php while( have_rows('case_study') ): the_row();
                  ?>
                  <h4><?php the_sub_field('case_study_title'); ?></h4>
                  <?php the_sub_field('case_study_content'); ?>
                <?php endwhile; ?>
              </div>
             <?php endif; ?>
           </section>

           <section class="payment-options">
             <h2>Payment Options</h2>
           </section>
         </div><!--end of right col -->
     </div>
 <?php
}



genesis();
